import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginComponent } from '../login/login.component'
import { DashboardComponent } from '../dashboard/dashboard.component'
import { RegisterComponent } from '../register/register.component';
import { CreateBoardComponent } from '../create-board/create-board.component';
import { ViewBoardComponent } from '../view-board/view-board.component';
import { AuthGuard } from '../guards/auth.guard';



const appRoutes: Routes = [
   { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
   { path: 'login', component: LoginComponent },
   { path: 'view-board', component: ViewBoardComponent },
   { path: 'register', component: RegisterComponent },
   { path: 'create-board', component: CreateBoardComponent, canActivate: [AuthGuard] },

   // otherwise redirect to home
   { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
     appRoutes,
       { enableTracing: true } // <-- debugging purposes only
     )
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {

}
