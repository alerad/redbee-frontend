import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor, UrlInterceptor, ResponseInterceptor } from './interceptors';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ViewBoardComponent } from './view-board/view-board.component';
import { CreateBoardComponent } from './create-board/create-board.component';
import { HeaderComponent } from './header/header.component';
import { InterestsApiService, AuthService, UserApiService, BoardApiService, BoardService } from './services/index';
import { BoardDetailsComponent } from './board-details/board-details.component';
import { TweetDetailsComponent } from './tweet-details/tweet-details.component';
import { ToastComponent } from './toast/toast.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    ViewBoardComponent,
    CreateBoardComponent,
    HeaderComponent,
    BoardDetailsComponent,
    TweetDetailsComponent,
    ToastComponent
  ],
  imports: [
    AppRoutingModule,
    FormsModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    UserApiService,
    InterestsApiService,
    AuthService,
    BoardService,
    BoardApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UrlInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
