import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BoardService } from '../services/board.service';

@Component({
  selector: 'app-board-details',
  templateUrl: './board-details.component.html',
  styleUrls: ['./board-details.component.scss']
})
export class BoardDetailsComponent implements OnInit {

  @Input()
  private board;

  constructor(private router: Router, private boardService: BoardService) { }

  ngOnInit() {
  }

  showBoard() {
    this.boardService.setSelectedBoard(this.board);
    this.router.navigate(['/view-board']);
  }

}
