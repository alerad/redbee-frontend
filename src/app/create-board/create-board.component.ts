import { Component, OnInit } from '@angular/core';
import { BoardApiService } from '../services/board-api.service';
import * as M from "materialize-css/dist/js/materialize";

@Component({
  selector: 'app-create-board',
  templateUrl: './create-board.component.html',
  styleUrls: ['./create-board.component.scss']
})
export class CreateBoardComponent implements OnInit {

  private board : any = {}
  private chips: any
  constructor(private boardApi: BoardApiService) { }

  ngOnInit() {
      const elems = document.querySelectorAll('.chips');
      this.chips = M.Chips.init(elems, {
          placeholder : "#cocina",
          secondaryPlaceholder: "@HaroldMcGee",
          onChipAdd: function(chips, chip){
            var chipText = chip.childNodes[0].data;
            if (chipText.charAt(0) != "@" && chipText.charAt(0) != "#"){
              chip.remove()
              //TODO Alert service que diga que no podes
            }
          }
       });
  }

  createBoard() {
      this.setBoardInterests();
      this.boardApi.post(this.board).subscribe(res=>{
        console.log(res);
      });
  }

  setBoardInterests() {
    var chipsArray = []
    this.chips[0].chipsData.map(chip =>
      chipsArray.push(chip.tag)
    )
    this.board.interests = chipsArray;
  }


}
