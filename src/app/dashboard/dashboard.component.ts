import { Component, OnInit } from '@angular/core';
import { BoardDetailsComponent } from '../board-details/board-details.component'
import { BoardApiService } from '../services/board-api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  entryComponents: [
    BoardDetailsComponent
  ],
  providers: [

  ]
})
export class DashboardComponent implements OnInit {

  private boards : any = []

  constructor(private boardApi: BoardApiService, private router: Router) {

  }

  ngOnInit() {
      this.boardApi.getAll().subscribe(boards => {
        this.boards = boards;
      })
  }

  addBoard() {
    this.router.navigate(['/create-board'])
  }

}
