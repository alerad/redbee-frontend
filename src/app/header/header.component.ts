import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services'
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private loggedIn;
  constructor(private auth: AuthService, private router: Router) {
      this.loggedIn = auth.isLoggedIn();
   }

  ngOnInit() {
  }

  logOut() {
    this.auth.logout();
    this.router.navigate(['/login'])
  }

  home() {
    this.router.navigate(['/'])
  }

}
