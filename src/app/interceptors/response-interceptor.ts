import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor , HttpResponse, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AlertService } from '../services'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'

@Injectable({
  providedIn: 'root'
})
export class ResponseInterceptor implements HttpInterceptor {

  constructor(private alert: AlertService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).do(event => {
    }, (error: HttpErrorResponse) => {
        this.sendAlert(error.error.developerMessage); // body
    });
  }

  sendAlert(error) {
    console.log(error +" Mandando alerta")
    this.alert.error(error);
  }




}
