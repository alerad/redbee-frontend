import { TestBed, inject } from '@angular/core/testing';

import { UrlInterceptor } from './url-interceptor';

describe('UrlInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlInterceptor]
    });
  });

  it('should be created', inject([UrlInterceptor], (service: UrlInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
