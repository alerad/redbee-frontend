export class Board {
  id: string;
  user: string;
  interests: string[];
  title: string;
}
