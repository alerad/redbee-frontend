import { Component, OnInit } from '@angular/core';
import { UserApiService } from '../services/user-api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private user: any = {}

  constructor(private userApi: UserApiService, private router: Router) { }

  ngOnInit() {

  }

  registerUser() {
      this.userApi.create(this.user).subscribe(
        data => {
            this.router.navigate(['/login']);
        },
        error => {
        }
      );
  }
}
