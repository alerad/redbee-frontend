import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private eventStream = new Subject();

  constructor() { }

  error(error: string) {
      this.eventStream.next({color: 'red lighten-1', text: error})
  }

  success(error: string) {
      this.eventStream.next({color: 'light-green', text: error})
  }

  getObservable(): Observable<any> {
      return this.eventStream.asObservable();
  }

}
