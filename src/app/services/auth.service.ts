import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from "rxjs/observable";
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(user: User) {
    return this.http.post<any>('/users/auth', { userName: user.userName, password: user.password }).map(user => {
        if (user.token) {
            localStorage.setItem('userData', JSON.stringify(user));
        }
        return user;
    })
  }

  isLoggedIn() {
    return localStorage.getItem('userData') != null
  }

  logout() {
    localStorage.removeItem('userData');
  }

}
