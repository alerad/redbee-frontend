import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from "rxjs/observable";
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class BoardApiService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get('/boards')
  }

  post(board: any) {
    return this.http.post('/boards', board)
  }
}
