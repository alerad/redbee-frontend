import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  constructor() { }

  getSelectedBoard() {
    return localStorage.getItem("selectedBoard");
  }

  setSelectedBoard(board: any) {
    return localStorage.setItem("selectedBoard", JSON.stringify(board));
  }

}
