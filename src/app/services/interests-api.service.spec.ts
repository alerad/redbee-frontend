import { TestBed, inject } from '@angular/core/testing';

import { InterestsApiService } from './interests-api.service';

describe('InterestsApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterestsApiService]
    });
  });

  it('should be created', inject([InterestsApiService], (service: InterestsApiService) => {
    expect(service).toBeTruthy();
  }));
});
