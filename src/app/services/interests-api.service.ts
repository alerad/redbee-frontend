import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from "rxjs/observable";
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class InterestsApiService {

  constructor(private http: HttpClient) { }

  getTweets(board: any, cache: boolean)  {
    return this.http.post('/interests', {interests: board.interests, withCache: cache});
  }

}
