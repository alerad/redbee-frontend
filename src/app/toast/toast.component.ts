import { Component, OnInit } from '@angular/core';
import { AlertService } from '../services'
import * as M from "materialize-css/dist/js/materialize";


@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {

  constructor(private alert: AlertService) {
    alert.getObservable().subscribe( (message:any) =>
      M.toast({html: message.text, classes: message.color})
    )
  }

  ngOnInit() {
  }

}
