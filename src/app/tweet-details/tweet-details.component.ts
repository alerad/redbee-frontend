import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tweet-details',
  templateUrl: './tweet-details.component.html',
  styleUrls: ['./tweet-details.component.scss']
})
export class TweetDetailsComponent implements OnInit {

  @Input()
  private tweet;

  constructor() { }

  ngOnInit() {

  }

}
