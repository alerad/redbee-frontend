import { Component, OnInit } from '@angular/core';
import { BoardService } from '../services/board.service';
import { InterestsApiService } from '../services/interests-api.service';
import { Observable } from "rxjs/observable";
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-view-board',
  templateUrl: './view-board.component.html',
  styleUrls: ['./view-board.component.scss']
})
export class ViewBoardComponent implements OnInit {

  private board: any;
  private tweets: any = [];
  private polling : any;

  constructor(private boardService: BoardService, private interestApi: InterestsApiService) {
    this.board = JSON.parse(boardService.getSelectedBoard());

    this.polling = this.observeTweets();
  }

  ngOnInit() {
    this.interestApi.getTweets(this.board, false).subscribe((interests: any) => {
      this.tweets = (interests)
    })
  }

  observeTweets() {
    return Observable.interval(30000)
        .switchMap(() => this.interestApi.getTweets(this.board, true))
        .subscribe((interests: any) => {
          this.tweets = this.tweets.concat(interests)
          console.log(this.tweets)
        })
  }

}
